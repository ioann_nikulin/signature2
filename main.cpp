#include <iostream>

using namespace std;

void i()
{
    cout << "  O   " << endl;
    cout << "      " << endl;
    cout << "  |-- " << endl;
    cout << "  |   " << endl;
    cout << "  |   " << endl;
    cout << " ---- " << endl;
}

void v()
{
    cout << "      " << endl;
    cout << "\\    /" << endl;
    cout << " \\  / " << endl;
    cout << "  \\/  " << endl;
    cout << "      " << endl;
    cout << "      " << endl;
}

void a()
{
    cout << "        " << endl;
    cout << "  ---\\  " << endl;
    cout << "  ____| " << endl;
    cout << " /    | " << endl;
    cout << " \\____| " << endl;
    cout << "        " << endl;
}

void n()
{
    cout << "|    |" << endl;
    cout << "|\\   |" << endl;
    cout << "| \\  |" << endl;
    cout << "|  \\ |" << endl;
    cout << "|   \\|" << endl;
    cout << "|    |" << endl;
}

int main()
{
	i();
	v();
    a();
    n();
    return 0;
}
